#include "ConcreteObserverA.h"
#include "Subject.h"

void ConcreteObserverA::update(Subject* sj) {
    cout<<"----------我要煮着吃-------------"<<endl;
    cout<<sj->getState()<<endl;
    cout<<"-----------------------"<<endl;
}
