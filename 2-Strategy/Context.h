#ifndef CONTEXT_H
#define CONTEXT_H

class Strategy;
class Context {

private:
	Strategy* _stg;

public:
	void do_method();

	Context(Strategy* stg) {
	    _stg = stg;
	};
};

#endif
