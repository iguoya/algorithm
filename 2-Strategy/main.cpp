#include <iostream>
#include "Strategy.h"
#include "ConcreteStrategyA.h"
#include "ConcreteStrategyB.h"
#include "Context.h"

int main() {
    std::cout << "Hello, World!" << std::endl;
    Strategy *stg = new ConcreteStrategyA();

    Context *ct = new Context(stg);
    ct->do_method();


    Context *ct2 = new Context(new ConcreteStrategyB());
    ct2->do_method();

    return 0;
}
