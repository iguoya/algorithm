#ifndef STRATEGY_H
#define STRATEGY_H

class Strategy {


public:
	virtual void traffic_type() = 0;
};

#endif
